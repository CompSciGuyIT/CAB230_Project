<!DOCTYPE html>
<?php 
    session_start(); 
    include 'pdo.inc';
?>

<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Home</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    <div id="header_menu">
                        <a class="menu" href="signup.php">sign up</a>
                        <a class="menu" href="login.php">log in</a>
                    </div>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Name Form -->

            <div id="modal-p" class="modal">
                <!-- Modal content -->
                <div class="modal-content-p">
                    <form class="searchbox" method="post">
                        <div class="searchName">Enter park name:</div>
                        <?php
                            echo '<div id="search-field">';
                                echo '<input id="input-search" type="text" name="search_name">';
                            echo '</div>';
                            echo '<input type="submit" value="Confirm" class="confirm-button">';
                            echo '<input type="cancel" value="Cancel" class="cancel-button" onclick="window.location=\'index.php\'">';
                        ?>
                    </form>
                </div>
            </div>
            
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>