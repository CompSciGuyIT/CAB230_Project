<?php
    session_start();
    echo "Logout successful";

    // destroys all session data
    session_destroy();

    header("Location: http://{$_SERVER['HTTP_HOST']}/login.php");
?>