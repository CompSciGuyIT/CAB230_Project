<?php 
    session_start(); 
    include 'pdo.php';
    include 'functions.php';

    try {
        echo "<script type='text/javascript'>document.getElementsByName('search_name').placeholder='Enter Park Name';</script>";
        // Get user input and convert it to database format
        $park_name = $_GET['search_name'];
        $park_name = format_name_search($park_name);

        // Query database with prepared statement
        $result = $pdo->prepare('SELECT * FROM parks WHERE Name = :park_name');
        $result->bindValue(':park_name', $park_name);
        $result->execute();

        // Redirect if a match is found
        if ($result->rowCount() > 0) {
            $name = $result["Name"];
            header("Location:http://{$_SERVER['HTTP_HOST']}/review.php");
            exit();
        } else {
            echo "<script type='text/javascript'>document.getElementsByName('search_name').placeholder='No Park Found';</script>";
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Home</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Name Form -->

            <div id="modal-p" class="searchArea">
                <div class="modal-content-p">
                    <form class="searchbox" method="post">
                        <div class="searchName">Enter park name:</div>
                            <div id="search-field">';
                                <input id="input-search" type="text" name="search_name">
                            </div>
                            <input type="submit" value="Confirm" class="confirm-button">
                            <input type="cancel" value="Cancel" class="cancel-button" onclick="window.location='index.php'">
                    </form>
                </div>
            </div>
            
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>