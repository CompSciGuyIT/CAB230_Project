<!DOCTYPE html>
<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Home</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">
        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    <div id="header_menu">
                        <a class="menu" href="signup.php">sign up</a>
                        <a class="menu" href="login.php">log in</a>
                    </div>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Main Menu -->

            <div class="content-form">           
                <div id="searchform">
                    <div id="searchform-title">Find your park</div>
                    <div id="searchform-subtitle">Search options:</div>
                    <div class="button" id="auto-button">Closest To You</div>
                    <div class="button" id="suburb-button">By Suburb</div>
                    <div class="button" id="park-button">By Park Name</div>
                    <div class="button" id="rating-button">By Rating</div>
                </div>
            </div>

            <!-- The Modals -->

            <div id="modal-s" class="modal">
                <!-- Modal content -->
                <div class="modal-content-s">
                    <div class="searchbox">
                        <div class="searchSuburb">Suburb List</div>

                        <div class="dropdown-content">
                            <a href="search_results.php">ACACIA RIDGE</a>
                            <a href="search_results.php">ALBION</a>
                            <a href="search_results.php">ALDERLEY</a>
                            <a href="search_results.php">ALGESTER</a>
                            <a href="search_results.php">ANNERLEY</a>
                            <a href="search_results.php">ANSTEAD</a>
                            <a href="search_results.php">ARCHERFIELD</a>
                            <a href="search_results.php">ASCOT</a>
                            <a href="search_results.php">ASHGROVE</a>
                            <a href="search_results.php">ASPLEY</a>
                            <a href="search_results.php">AUCHENFLOWER</a>
                            <a href="search_results.php">BALD HILLS</a>
                            <a href="search_results.php">BALMORAL</a>
                            <a href="search_results.php">BANYO</a>
                            <a href="search_results.php">BARDON</a>
                            <a href="search_results.php">BELLBOWRIE</a>
                            <a href="search_results.php">BELMONT</a>
                            <a href="search_results.php">BOONDALL</a>
                            <a href="search_results.php">BOWEN HILLS</a>
                            <a href="search_results.php">BRACKEN RIDGE</a>
                            <a href="search_results.php">BRIDGEMAN DOWNS</a>
                            <a href="search_results.php">BRIGHTON</a>
                            <a href="search_results.php">BRISBANE</a>
                            <a href="search_results.php">BRISBANE CITY</a>
                            <a href="search_results.php">BROOKFIELD</a>
                            <a href="search_results.php">BULIMBA</a>
                            <a href="search_results.php">BULWER</a>
                            <a href="search_results.php">BURBANK</a>
                            <a href="search_results.php">CALAMVALE</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-p" class="modal">
                <!-- Modal content -->
                <div class="modal-content-p">
                    <form class="searchbox">
                        <div class="searchName">Enter park name:</div>
                        <div id="search-field">
                            <input id="input-search" type="text" name="search_name">
                        </div>
                        <div id="name-confirm-button" class="confirm-button">Confirm</div>
                        <input type="reset" value="Cancel" id="name-cancel-button" class="cancel-button">
                    </form>
                </div>
            </div>

            <div id="modal-r" class="modal">
                <!-- Modal content -->
                <div class="modal-content-r">
                    <form class="searchbox">
                        <div class="searchRating">Choose minimum rating:</div>
                        <div id="rating-buttons" class="rating_choice">
                            <input id="rating5" type="radio" name="rating" value="5">
                            <label for="rating5"></label>
                            <input id="rating4" type="radio" name="rating" value="4">
                            <label for="rating4"></label>
                            <input id="rating3" type="radio" name="rating" value="3">
                            <label for="rating3"></label>
                            <input id="rating2" type="radio" name="rating" value="2" checked>
                            <label for="rating2"></label>
                            <input id="rating1" type="radio" name="rating" value="1">
                            <label for="rating1"></label>
                        </div>
                        <div id="rating-confirm-button" class="confirm-button">Confirm</div>
                        <input type="reset" value="Cancel" id="rating-cancel-button" class="cancel-button">
                    </form>
                </div>
            </div>
            
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>