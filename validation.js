// Checks that passwords match
function checkPassword() {
    var pass1 = document.forms["myForm"]["pword1"].value;
    var pass2 = document.forms["myForm"]["pword2"].value;
    var pass1bool = true;
    var pass2bool = true;
    var passMatch = true
    
    // Regex to test for
    var regex_a = /[a-z]/;      // lower case
    var regex_A = /[A-Z]/;      // upper case
    var regex_1 = /[0-9]/;      // numerical
    var regex_sym = /\W/;       // symbols
    var regex_spc = /\s/;       // spaces

    // Test validity of first entry
    if ((pass1.length < 16) || (!(regex_a.test(pass1))) || (!(regex_A.test(pass1))) || (!(regex_1.test(pass1))) || (!(regex_sym.test(pass1))) || (regex_spc.test(pass1))){
        document.getElementById("pass1invalid").style.visibility = "visible";
        pass1bool = false;
    }

    // Test for a match
    if (pass1 != pass2) {
        document.getElementById("matchFail").style.visibility = "visible";
        passMatch = false;
    }
    
    return (pass1bool && passMatch);
}

// Checks for valid email
function checkEmail() {
    var myEmail = document.forms["myForm"]["email"].value;
    var atpos = myEmail.indexOf("@");
    var dotpos = myEmail.lastIndexOf(".");
    if ((myEmail.length == 0) || (atpos<1 || dotpos<atpos+2 || dotpos+2>=myEmail.length)){
        document.getElementById("emailFail").style.visibility = "visible";
        return false;
    }
    return true;
}
