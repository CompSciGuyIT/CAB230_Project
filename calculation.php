<?php
function location_distance($lat1, $lon1, $lat2, $lon2){
$lat_average = deg2rad( $lat1 + (($lat2 - $lat1) / 2) );
$lat_difference = deg2rad( $lat1 - $lat2 );
$lon_difference = deg2rad( $lon1 - $lon2 );
$curvature_radius_tmp = 1 - 0.00669438 * pow(sin($lat_average), 2);
$meridian_curvature_radius = 6335439.327 / sqrt(pow($curvature_radius_tmp, 3));
$prime_vertical_circle_curvature_radius = 6378137 / sqrt($curvature_radius_tmp);

$distance = pow($meridian_curvature_radius * $lat_difference, 2) + pow($prime_vertical_circle_curvature_radius * cos($lat_average) * $lon_difference, 2);
$distance = sqrt($distance);

$distance_unit = round($distance);
$distance_unit = round($distance_unit / 100);
$distance_unit = $distance_unit / 10;

return array("distance" => $distance, "distance_unit" => $distance_unit);
}
?>