<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    // Converts input to database format
    function format_name_search($text) {
        // Removes whitespces from both sides of a string
        $text = trim($text);

        // Removes backslashes
        $text = stripslashes($text);

        // Prevents browsers from using HTML characters as elements
        $text = htmlspecialchars($text);

        // Converts string to uppercase as is in database
        $text = strtoupper($text);
        return $text;
    }
?>