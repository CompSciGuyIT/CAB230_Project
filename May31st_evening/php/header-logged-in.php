<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
    echo '
    <div id="header">
        <div class="nograd">
            <a id="logo" href="../index.php">
                <img src="../images/logo.png" onerror="this.src="images/logo.png" alt="logo">
            </a>
            <div id="header_menu">
                <div class="menu">Welcome ',$_SESSION["user"],'</div>
                <a class="menu" href="logout.php">log out</a>
            </div>
            
        </div>
        <div id="h_grad"></div>
    </div>
    ';
?>