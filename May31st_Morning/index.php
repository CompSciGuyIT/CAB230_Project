<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php 
    session_start();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Home</title>
        <link href="css/project.css" rel="stylesheet" type="text/css" />
        <script async src="js/project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include "php/header-logged-in.php";
                } else {
                    include "php/header-logged-out.php";
                }
            ?>
            <!-- Main Menu -->

            <div class="content-form">
                <div id="searchform">
                    <div id="searchform-title">Find your park</div>
                    <div id="searchform-subtitle">Search options:</div>
                    <input type="submit" class="button" value="Closest To You" onclick="window.location='php/by_nearest.php'" />
                    <input type="submit" class="button" value="By Suburb" onclick="window.location='php/by_suburb.php'" />
                    <input type="submit" class="button" value="By Park Name" onclick="window.location='php/by_name.php'" />
                    <input type="submit" class="button" value="By Rating" onclick="window.location='php/by_rating.php'">
                </div>
            </div>

            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>